<?php

/* Saves the crowd favorite team member with custom fields. */
add_action('save_post', 'cftm_metabox_save');
function cftm_metabox_save($post_id) {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
    if (!isset($_POST['cftm_metabox_nonce']) || !wp_verify_nonce($_POST['cftm_metabox_nonce'], '_cftm_metabox_nonce')) return;
    if (!current_user_can('edit_post', $post_id)) return;

    if (isset($_POST['cftm_metabox_position']))
        update_post_meta($post_id, 'cftm_metabox_position', sanitize_text_field($_POST['cftm_metabox_position']));
    if (isset($_POST['cftm_metabox_twitter_url']))
        update_post_meta($post_id, 'cftm_metabox_twitter_url', sanitize_text_field($_POST['cftm_metabox_twitter_url']));
    if (isset($_POST['cftm_metabox_facebook_url']))
        update_post_meta($post_id, 'cftm_metabox_facebook_url', sanitize_text_field($_POST['cftm_metabox_facebook_url']));
}