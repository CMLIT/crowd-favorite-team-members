<?php

/* Registers the crowd favorite team member post type. */
add_action('init', 'register_cftm_type');
function register_cftm_type() {

    /* Defines labels. */
    $labels = array(
        'name' => _x('Team Members', 'Post Type General Name', CFTM_TXTDM),
        'singular_name' => _x('Team Member', 'Post Type Singular Name', CFTM_TXTDM),
        'menu_name' => __('Team Members', CFTM_TXTDM),
        'name_admin_bar' => __('Team Member', CFTM_TXTDM),
        'all_items' => __('All Team Members', CFTM_TXTDM),
        'add_new_item' => __('Add New Team Member', CFTM_TXTDM),
        'add_new' => __('New Team Member', CFTM_TXTDM),
        'new_item' => __('New Item', CFTM_TXTDM),
        'edit_item' => __('Edit Team Member', CFTM_TXTDM),
        'update_item' => __('Update Team Member', CFTM_TXTDM),
        'view_item' => __('View Team Member', CFTM_TXTDM),
        'view_items' => __('View Items', CFTM_TXTDM),
        'search_items' => __('Search Team Members', CFTM_TXTDM),
        'not_found' => __('No Team Members found', CFTM_TXTDM),
        'not_found_in_trash' => __('No Team Members found in Trash', CFTM_TXTDM),
        'featured_image' => __('Featured Image', CFTM_TXTDM),
        'set_featured_image' => __('Set featured image', CFTM_TXTDM),
        'remove_featured_image' => __('Remove featured image', CFTM_TXTDM),
        'use_featured_image' => __('Use as featured image', CFTM_TXTDM)
    );

    /* Defines permissions. */
    $args = array(
        'label' => __('Team Member', CFTM_TXTDM),
        'description' => __('Team Member information pages.', CFTM_TXTDM),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array('cf_department'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-groups'
    );

    /* Registers post type. */
    register_post_type('cf_team_members', $args);
}