<?php

/* Enqueues front scripts. */
add_action('wp_enqueue_scripts', 'add_cftm_scripts', 99);
function add_cftm_scripts() {
    /* Front end CSS. */
    wp_enqueue_style('cftm_front_styles', plugins_url('../public/css/cftm-front.css', __FILE__));

    /* Front end JS */
    wp_enqueue_script('cftm_front_js', plugins_url('../public/js/cftm-front.js', __FILE__), array('jquery'));
}