<?php

/* Registers the crowd favorite custom fields. */
add_action('add_meta_boxes', 'cftm_metabox_add_meta_box');
function cftm_metabox_get_meta($value) {
    global $post;

    $field = get_post_meta($post->ID, $value, true);
    if (!empty($field)) {
        return is_array($field) ? stripslashes_deep($field) : stripslashes(wp_kses_decode_entities($field));
    } else {
        return false;
    }
}

function cftm_metabox_add_meta_box() {
    add_meta_box(
        'cftm_metabox',
        __('CFTM Metabox', CFTM_TXTDM),
        'cftm_metabox_html',
        'cf_team_members',
        'normal',
        'high'
    );
}

function cftm_metabox_html() {
    wp_nonce_field('_cftm_metabox_nonce', 'cftm_metabox_nonce'); ?>

    <p class="cftm_metabox_field">
        <label for="cftm_metabox_position"><?php _e('Position', CFTM_TXTDM); ?></label><br>
        <input type="text" name="cftm_metabox_position" id="cftm_metabox_position"
               value="<?php echo cftm_metabox_get_meta('cftm_metabox_position'); ?>">
    </p>
    <p class="cftm_metabox_field">
        <label for="cftm_metabox_twitter_url"><?php _e('Twitter URL', CFTM_TXTDM); ?></label><br>
        <input type="text" name="cftm_metabox_twitter_url" id="cftm_metabox_twitter_url"
               value="<?php echo cftm_metabox_get_meta('cftm_metabox_twitter_url'); ?>">
    </p>
    <p class="cftm_metabox_field">
        <label for="cftm_metabox_facebook_url"><?php _e('Facebook URL', CFTM_TXTDM); ?></label><br>
        <input type="text" name="cftm_metabox_facebook_url" id="cftm_metabox_facebook_url"
               value="<?php echo cftm_metabox_get_meta('cftm_metabox_facebook_url'); ?>">
    </p>

    <?php
}
?>