<?php

/* Enqueues admin scripts. */
add_action('admin_enqueue_scripts', 'add_admin_cftm_style');
function add_admin_cftm_style() {

    /* Admin CSS. */
    wp_enqueue_style('cftm_admin_styles', plugins_url('../admin/css/cftm-admin.css', __FILE__));
}