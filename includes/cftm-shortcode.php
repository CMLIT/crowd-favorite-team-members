<?php

/* Handles CFT team members shortcode. */
add_shortcode("cf_team_members", "cf_team_members_sc");
function cf_team_members_sc()
{

    global $post;

    /* Gets the team. */
    $args = array(
        'post_type' => 'cf_team_members',
        'orderby' => 'title',
        'order' => 'ASC',
        'numberposts' => 12
    );

    $custom_posts = get_posts($args);

    $team_view = '';
    $team_view .= '<div class="cftm-container">';
    foreach ($custom_posts as $post) : setup_postdata($post);
        $team_view .= '<div class="cftm-card-outer">';
        $team_view .= '    <article id="post-' . get_the_ID() . '">';
        $team_view .= '        <div class="cftm-card">';
        $team_view .= '            <a href="' .  esc_url( apply_filters( 'the_permalink', get_permalink( $post ), $post ) ) . '">';
        $team_view .= '                 <div class="cftm-profile-image" style="background-image: url(' . esc_url(get_the_post_thumbnail_url()) . ')"></div>';
        $team_view .= '            </a>';
        $team_view .= '            <div class="cftm-title">' . get_the_title() . '</div>';
        $team_view .= '            <div class="cftm-position">' . (esc_html(get_post_meta(get_the_ID(), 'cftm_metabox_position', true)) ?: 'unknown') . '</div>';
        $team_view .= '            <div class="cftm-social-media">';
        if (!empty($facebook_url = esc_html(get_post_meta(get_the_ID(), 'cftm_metabox_facebook_url', true)))):
            $team_view .= '            <a class="dashicons dashicons-facebook" href="' . $facebook_url . '"></a>';
        endif;
        if (!empty($twitter_url = esc_html(get_post_meta(get_the_ID(), 'cftm_metabox_twitter_url', true)))):
            $team_view .= '            <a class="dashicons dashicons-twitter" href="' . $twitter_url . '"></a>';
        endif;
        $team_view .= '        </div>';
        $team_view .= '            <div class="cftm-description" data-id="' . get_the_ID() . '">';
        if ('' !== get_the_content()):
            $team_view .= get_the_content();
        endif;
        $team_view .= '            </div>';
        $team_view .= '            <div class="cftm-button-container">';
        $team_view .= '                <button type="button" class="btn btn-primary cftm-read-more-button" id="" data-id="' . get_the_ID() . '">Read more</button>';
        $team_view .= '            </div>';
        $team_view .= '        </div>';
        $team_view .= '    </article>';
        $team_view .= '</div>';
    endforeach;

    $team_view .= '</div>';

    wp_reset_postdata();

    echo $team_view;

}

?>