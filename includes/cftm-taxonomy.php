<?php

/* Registers the crowd Department taxonomy. */
add_action('init', 'register_cftm_taxonomy');
function register_cftm_taxonomy() {
    /* Defines labels. */
    $labels = array(
        'name' => _x('Departments', 'Taxonomy General Name', CFTM_TXTDM),
        'singular_name' => _x('Department', 'Taxonomy Singular Name', CFTM_TXTDM),
        'menu_name' => __('Department', CFTM_TXTDM),
        'all_items' => __('All Departments', CFTM_TXTDM),
        'new_item_name' => __('New Department Name', CFTM_TXTDM),
        'add_new_item' => __('Add New Department', CFTM_TXTDM),
        'edit_item' => __('Edit Department', CFTM_TXTDM),
        'update_item' => __('Update Department', CFTM_TXTDM),
        'view_item' => __('View Item', CFTM_TXTDM),
        'separate_items_with_commas' => __('Separate Departments with commas', CFTM_TXTDM),
        'add_or_remove_items' => __('Add or remove Departments', CFTM_TXTDM),
        'choose_from_most_used' => __('Choose from the most used Departments', CFTM_TXTDM),
        'popular_items' => __('Popular Items', CFTM_TXTDM),
        'search_items' => __('Search Departments', CFTM_TXTDM),
        'not_found' => __('Not Found', CFTM_TXTDM),
        'no_terms' => __('No items', CFTM_TXTDM),
        'items_list' => __('Items list', CFTM_TXTDM),
        'items_list_navigation' => __('Items list navigation', CFTM_TXTDM),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
    );

    /* Registers taxonomy. */
    register_taxonomy('cf_department', array('cf_team_members'), $args);
}