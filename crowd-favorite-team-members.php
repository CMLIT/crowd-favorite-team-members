<?php
/*
 * Plugin Name: Crowd Favorite Team Members
 * Plugin URI:
 * Description: This is my first attempt on writing a custom Plugin for getting an amazing job at Crowd Favorite
 * Version: 1.0.0
 * Author: Loredana Cimpeanu
 * Licence: GPLv2 or later
 * Text Domain: Crowd Favorite Team Members
 */

/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/* Defines plugin's text domain. */
define( 'CFTM_TXTDM', 'crowd-favorite-team-members' );

/**
 * Set a transient used for redirection upon activation.
 *
 * @since 1.0.0
 */
function cftm_activation_redirect() {
    // Bail if activating from network, or bulk.
    if ( is_network_admin() ) {
        return;
    }

    // Add the transient to redirect.
    set_transient( 'cftm_activation_redirect', true, 30 );
}
add_action( 'activate_' . plugin_basename( __FILE__ ), 'cftm_activation_redirect' );

/**
 * Redirect user to CFTM about page upon plugin activation.
 *
 * @since 1.0.0
 */
function cftm_make_activation_redirect() {

    if ( ! get_transient( 'cftm_activation_redirect' ) ) {
        return;
    }

    delete_transient( 'cftm_activation_redirect' );

    // Bail if activating from network, or bulk.
    if ( is_network_admin() ) {
        return;
    }

    if ( ! cftm_is_new_install() ) {
        return;
    }

    // Redirect to CFTM about page.
    exit( wp_redirect( admin_url( 'edit.php?post_type=cf_team_members&page=cftm_menu_help' ) ) );
}
add_action( 'admin_init', 'cftm_make_activation_redirect', 1 );

/**
 * Check whether or not we're on a new install.
 *
 * @since 1.0.0
 *
 * @return bool
 */
function cftm_is_new_install() {
    $new_or_not = true;
    $saved = get_option( 'cftm_new_install', '' );

    if ( 'false' === $saved ) {
        $new_or_not = false;
    }

    /**
     * Filters the new install status.
     *
     * Offers third parties the ability to override if they choose to.
     *
     * @since 1.5.0
     *
     * @param bool $new_or_not Whether or not site is a new install.
     */
    return (bool) apply_filters( 'cftm_is_new_install',  $new_or_not );
}

/**
 * Flush our rewrite rules on deactivation.
 *
 * @since 1.0.0
 *
 * @internal
 */
function cftm_deactivation() {
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'cftm_deactivation' );

/**
 * Register our text domain.
 *
 * @since 1.0.0
 *
 * @internal
 */
function cftm_load_textdomain() {
    load_plugin_textdomain( CFTM_TXTDM );
}
add_action( 'plugins_loaded', 'cftm_load_textdomain' );

/**
 * Load our main menu.
 *
 * @since 1.0.0
 *
 * @internal
 */
function cftm_menu_init()
{
    add_submenu_page('edit.php?post_type=cf_team_members', __('Help','team-members'), __('Help','team-members'), 'manage_options', 'cftm_menu_help', 'cftm_menu_help');
}
add_action( 'admin_menu', 'cftm_menu_init' );

/*
 * Includes the CFTM help menu page
 */
function cftm_menu_help(){
    include('includes/cftm-help.php');
}

/*
 * Includes the CFTM Scripts
 */
require_once('includes/cftm-front-scripts.php');
require_once('includes/cftm-admin-scripts.php');

/*
 * Includes the CFTM Post Type
 */
require_once('includes/cftm-post-type.php');

/*
 * Includes the CFTM Department Taxonomy
 */
require_once('includes/cftm-taxonomy.php');


/*
 * Includes the CFTM Metaboxes
 */
require_once('includes/cftm-metaboxes.php');

/*
 * Saves the CFTM Post
 */
require_once('includes/cftm-save-post.php');

/*
 * Includes the CFTM ShortCode
 */
require_once('includes/cftm-shortcode.php');