(function($) {
    $(document).on('click', '.cftm-read-more-button', function () {
        var id = $(this).attr('data-id');
        $('.cftm-description[data-id=' + id + ']').toggle();
    });
})(jQuery);